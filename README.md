# ALTRAN

## Requirements

- Python 3
- pip 3

## Installation

Install python dependencies:

```
pip3 install -r requirements.txt
```

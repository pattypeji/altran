FROM python:3

RUN mkdir /app

WORKDIR /app

COPY requirements.txt /app/

RUN pip3 install -r /app/requirements.txt

COPY *.py /app/

ENTRYPOINT ["python3"]

CMD ["/app/HoeWarmIsHetInDelft.py"]

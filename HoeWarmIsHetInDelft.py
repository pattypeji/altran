# -*- coding: utf-8 -*-
__author__ = 'pattypeji'

import requests
import shutil

from bs4 import BeautifulSoup
from variables import *

req = requests.get(URL)
status_code = req.status_code
if status_code == 200:
    html = BeautifulSoup(req.text, 'html.parser')
    aux = html.get_text()
    data = aux.split()[4]
    print (data + ' degrees celsius')
else:
    print ("Web Status Code " + status_code)
